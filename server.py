# first of all import the socket library
import socket			
import json
# next create a socket object
s = socket.socket()		
print ("Socket successfully created")

# reserve a port on your computer in our
# case it is 12345 but it can be anything
port =  4000			

# Next bind to the port
# we have not typed any ip in the ip field
# instead we have inputted an empty string
# this makes the server listen to requests
# coming from other computers on the network

# log_queue = ["random 1", "random3", "error error error eroor", "segment fault"]
log_queue =["session closed for user <*>"]
s.bind(('', port))		
print ("socket binded to %s" %(port))
data = {
	"Machine_ID": "VM1",
	"log": "session closed for user <*>"
}
JSON = json.dumps(data)
# put the socket into listening mode
s.listen(5)	
print ("socket is listening")		

# a forever loop until we interrupt it or
# an error occurs
while True:

# Establish connection with client.
	c, addr = s.accept()	
	print ('Got connection from', addr )

	# if len(log_queue) == 0:
	# 	continue

# send a thank you message to the client. encoding to send byte type.
	c.send(JSON.encode())
# Close the connection with the client
	c.close()

	del log_queue[0]

# Breaking once connection closed
	break

'''
1. Log Taken from message queue (socket)
2. Log Proessed
3. API Call Made to Result Cache if error


^  ->          Log Fetched     -> 
|                            Log Processed
API Call MAde                <-
'''
import time
from copy import Error
from log_processing import LogChecker, Graph
import requests
import pickle
from pymongo import MongoClient
class Processor:
    def __init__(self, g_obj, l_obj):
        self.graph_obj = g_obj
        self.log_check_obj = l_obj
    def start_process(self, logs):    
        # self.log_check_obj.load_graph_obj(self.graph_obj)
        f = self.log_check_obj.Generate_Predictions(logs)
        print(f)
        return f

def load_dictionary():
    dict_ = None
    with open("dictionary.pkl","rb") as f:
            dict_ = pickle.load(f)
    return dict_

mq_addr = "http://message-queue.com:5001/fetch/log-routes"

def load_log():
    mq_resp = requests.get(mq_addr)
    log_val = mq_resp.text
    print("log_val: " + log_val)
    return parsing_the_log(log_val=log_val)


# Returns [id of the machine, value of the log] 
def parsing_the_log(log_val : str) -> list:
    # Format of log_val => machineId__=__logValue
    first_break_pt = -1
    log_start_index = 0
    ocurences = 0
    for ind1 in range(len(log_val)):
        if first_break_pt == -1 and log_val[ind1] == "_":
            first_break_pt = ind1
        if log_val[ind1] == "_":
            ocurences += 1
        if ocurences == 4:
            log_start_index = ind1 + 1
            break
    return [log_val[:first_break_pt], log_val[log_start_index:]]

class DbOps:
    def send_to_cache(self, machine_id, results, cs_uri):
        requests.post(cs_uri + "/store/log-result", json={"machine_id": machine_id, "result" : results})
    def upload_log_to_db(self, machine_id, log_value):
        cluster = MongoClient("mongodb+srv://team86:team86@early-fault-detection.cnjve.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        db = cluster["logs"]
        log_doc = db[machine_id]
        log_doc.insert_one({"log-val" : log_value, "time" : str(time.time())})
    

# def send_to_cacher(log: str, machine_id : str, process_res : str):
#     '''
#     mach_id = req["machine_id"]
#     log_val = req["log_val"]
#     err_detect = req["err_detect"]
#     '''
#     data = {"log":log,"machine_id":machine_id,"process_res":process_res}
#     url = ""
#     payload = json.dumps(data)
#     headers = {
#         "Content-Type":'application/json'
#     }
#     res = requests.request("POST",url,headers=headers,data = payload)
#     return res

if __name__ == '__main__':
    cache_service_uri = "http://cache-service.com:4999"
    graph_obj = Graph()
    log_check_obj = LogChecker()
    processor = Processor(graph_obj, log_check_obj)
    dictionary_mapping = load_dictionary()
    db_obj = DbOps()
    while True:
        machine_id, log_value = load_log()
        resp = None
        try:
            log_value = log_value.strip()
            log_template = dictionary_mapping[log_value]
            resp = processor.start_process(log_template)
            db_obj.send_to_cache(machine_id=machine_id, results=resp, cs_uri=cache_service_uri)
        except Exception as e:
            print("Exception Occured")
            resp = ""
        print(f"The response is => ",resp)
        try:
            db_obj.upload_log_to_db(machine_id=machine_id, log_value=log_value)
        except:
            print("Exception Occured in soft.py while uploading to mongodb")
        # Cache method called only if error detected. Checck condition yet to be implemented
        




        # send_to_cacher(log["log"],log["Machine_ID"],resp)
        # print(resp)
        # break
        # If event tags, these are sent to cache service
# checker = LogChecker()
# print(checker.Generate_Predictions("session closed for user <*>"))
# t = Processor(graph_obj,log_check_obj)
# print(t.start_process("Random 1"))

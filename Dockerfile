FROM frolvlad/alpine-python-machinelearning
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN python3 -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
CMD ["python","soft.py"]


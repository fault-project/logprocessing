# emergency contact
from tinydb import TinyDB
import json

class Emailer:
    database = None
    def __init__(self, database):
        self.database = database
    def load_db_info(self):
        print("Load DB Info from TinyDB")
        db = TinyDB("contact.json")
        resp = db.search(db.name == "email")
        return json.dumps(resp[0])
    def get_email_addrs(self, db_uri) -> list:
        print("Return Info from column email from db")
    def action(self) -> None:
        print("Sends Emails") 

class Object_Maker:
    maps = {
        "email" : Emailer()
    }
        



class Contact:
    service_map = {}
    
    def __init__(self):
        pass
    
    def __add_service_to_db(self, service_name, service_data):
        db = TinyDB("contact.json")
        db.insert({"name" : service_name, "data": service_data})
    
    def required_services_setup(self):
        inp1 = input("Print y to use email service : ")
        if inp1 == "y":
            inp2 = input("Give the database uri for emails : ")
            self.__add_service_to_db("email", inp2)
            print("Adding service name to DB")
    
    def load_all_services(self) -> list:
        # All services are loaded in the servie_map here
        service_list = []
        db = TinyDB("contact.json")
        services = db.all()
        for s in services:
            service_list.append(s["name"])
        return service_list

    def take_action(self) -> bool:
        print("action method called in every object here")
        services = self.load_all_services()

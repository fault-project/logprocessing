import pickle
import numpy as np
from scipy.sparse.csgraph import shortest_path

class Graph:
    graph = None
    def __init__(self):
        pass
    def get_graph(self):
        with open("graph.pkl","rb") as f:
            self.graph = pickle.load(f)
        return self.graph

class LogChecker:
    vectorizer = None
    ERROR_INDICES = None
    vector_array = None
    ERROR_DISTANCE = None
    graph_obj = None
    def __init__(self):
        pass
    # def load_graph_obj(self, graph_obj):
    #     self.graph_obj = graph_obj
    #     self.get_graph = self.graph_obj.get_graph()
    def load_vectorizer(self):
        with open("vectorizer.pkl","rb") as f:
            self.vectorizer = pickle.load(f)
    def load_error_indices(self):
        with open("error_indices.pkl","rb") as f:
            self.ERROR_INDICES = pickle.load(f)
    def load_vector_array(self):
        with open("vector_array.pkl","rb") as f:
            self.vector_array = pickle.load(f)
    def Model(self,x):
        # if not self.graph_obj:
        #     g = Graph()
        #     self.get_graph = g.get_graph()
        #     del g
        self.load_vector_array()
        self.ERROR_DISTANCE = [0]*len(self.ERROR_INDICES)
        count =0 
        for i in self.vector_array:
            if np.array_equal(i,x):
                break
            count+=1
        # Get the distances from other nodes (Djikstra's Algo)
        g= Graph()
        dist_matrix = shortest_path(csgraph=g.get_graph(), directed=False, indices=count,method="D")
        # print(dist_matrix)
        return dist_matrix
    def Generate_Predictions(self,log):
        self.load_vectorizer()
        self.load_error_indices()
        
        ERROR = ["E16", "E8","E17","E59","E4","E14"]
        x=np.squeeze(list(self.vectorizer.transform([log]).todense()))
        dist_matrix = self.Model(x)
        #Get the error distances from distance list based on indices obtained from error list
        for i in range(len(self.ERROR_INDICES)):
            self.ERROR_DISTANCE[i]=dist_matrix[self.ERROR_INDICES[i]]
        # club error distance with their event id
        error_distance_With_labels = []
        for i in range(len(self.ERROR_DISTANCE)):
            error_distance_With_labels.append([self.ERROR_DISTANCE[i],ERROR[i]])
        #Sort the above list
        error_distance_With_labels.sort(key= lambda x:x[0],reverse=True)
        #Get Top 4 predictions
        error_distance_With_labels = error_distance_With_labels[:4]
        #Get the predicted Event Ids
        labels_fault_detected = [i[1] for i in error_distance_With_labels]
        return error_distance_With_labels

#Testing the log
# checker = LogChecker()
# print(checker.Generate_Predictions("session closed for user <*>"))
# g = Graph()
# print(g.get_graph())
